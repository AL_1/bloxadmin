package com.bloxadmin.mysql;

import java.sql.*;

public class Database {

    private final Connection con;

    public Database(String url, String database, String user, String password) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection(url, user, password);
        execute("USE " + database);
    }

    public ResultSet query(String sql) throws SQLException {
        return con.createStatement().executeQuery(sql);
    }

    public boolean execute(String sql) throws SQLException {
        return con.createStatement().execute(sql);
    }

    public PreparedStatement prepare(String sql) throws SQLException {
        return con.prepareStatement(sql);
    }
}
