package com.bloxadmin.http.websocket;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Frame {

    private boolean fin;
    private boolean rsv1;
    private boolean rsv2;
    private boolean rsv3;
    private int opcode;
    private byte[] payload;

    public String debug() {
        return String.format("%b;%b;%b;%b;%d", fin, rsv1, rsv2, rsv3, opcode);
    }

    public byte[] getPayload() {
        return payload;
    }

    private static byte readByte(InputStream is) throws IOException {
        byte[] arr = new byte[1];
        is.read(arr, 0, 1);
        return arr[0];
    }

    public static Frame parseSocket(Socket socket) throws IOException {
        InputStream is = socket.getInputStream();
        Frame frame = new Frame();
        boolean masked;
        int byteCount, payloadLength;
        byte b;
        byte[] maskingKey;

        b = readByte(is);
        frame.fin = (b & 0x80) != 0;
        frame.rsv1 = (b & 0x40) != 0;
        frame.rsv2 = (b & 0x20) != 0;
        frame.rsv3 = (b & 0x10) != 0;
        frame.opcode = (byte) (b & 0x0F);

        b = readByte(is);
        masked = (b & 0x80) != 0;

        payloadLength = (byte) (0x7F & b);
        byteCount = 0;

        if (payloadLength == 0x7F) {
            // 8 byte extended payload length
            byteCount = 8;
        } else if (payloadLength == 0x7E) {
            // 2 bytes extended payload length
            byteCount = 2;
        }

        // Decode Payload Length
        while (--byteCount > 0) {
            b = readByte(is);
            payloadLength |= (b & 0xFF) << (8 * byteCount);
        }

        // TODO: add control frame payload length validation here

        maskingKey = null;
        if (masked) {
            // Masking Key
            maskingKey = new byte[4];
            is.read(maskingKey, 0, 4);
        }

        // TODO: add masked + maskingkey validation here

        // Payload itself
        frame.payload = new byte[payloadLength];
        is.read(frame.payload, 0, payloadLength);

        // Demask (if needed)
        if (masked) {
            for (int i = 0; i < frame.payload.length; i++) {
                frame.payload[i] ^= maskingKey[i % 4];
            }
        }

        return frame;
    }

    @Override
    public String toString() {
        return new String(payload, StandardCharsets.UTF_8);
    }
}
