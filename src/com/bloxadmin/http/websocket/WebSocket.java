package com.bloxadmin.http.websocket;

import com.bloxadmin.http.Request;
import com.bloxadmin.http.Response;
import com.bloxadmin.http.Server;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class WebSocket {

    private Server server = null;
    private List<Client> clients = new ArrayList<>();

    public WebSocket() {
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public Server getServer() {
        return server;
    }

    public boolean isWebsocket(Request request) {
        boolean iw = request.getHeader("Sec-WebSocket-Key") != null;
        if(iw)
            System.out.println("Is websocket");
        return iw;
    }

    public void handle(Socket socket, Request request) throws IOException, NoSuchAlgorithmException {
        clients.add(new Client(socket, request));
    }
}
