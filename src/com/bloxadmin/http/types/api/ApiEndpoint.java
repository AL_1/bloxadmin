package com.bloxadmin.http.types.api;

import com.bloxadmin.http.Method;

public class ApiEndpoint {

    private Method method;
    private ApiRunnable runnable;

    public ApiEndpoint(Method method, ApiRunnable runnable) {
        this.method = method;
        this.runnable = runnable;
    }

    public Method getMethod() {
        return method;
    }

    public ApiRunnable getRunnable() {
        return runnable;
    }
}
