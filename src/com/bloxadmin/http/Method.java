package com.bloxadmin.http;

public enum Method {
    GET, POST, PUT, DELETE, HEAD, OPTIONS, PATCH, OTHER;

    String name;

    Method() {
        name = this.name();
    }

    public String getName() {
        return name;
    }

    public Method setName(String name) {
        if (this == OTHER)
            this.name = name.toUpperCase();
        return this;
    }

    public static Method fromString(String str) {
        try {
            Method method = Method.valueOf(str);
            return method;
        } catch (Exception e) {
            return OTHER.setName(str);
        }
    }
}
